import binascii
import codecs


def gcdex(a, b):
    if a == 0:
        return b, 0, 1
    gcd, x, y = gcdex(b % a, a)
    return gcd, y - (b // a) * x, x


# Операция вычисления элемента обратного по модулю
def invmod(a, m):
    g, x, y = gcdex(a, m)
    return None if g > 1 else (x % m + m) % m


def factor(num):
    result = []
    d = 2
    while d ** 2 <= num:
        if num % d == 0:
            result.append(d)
            num //= d
        else:
            d += 1
    if num > 1:
        result.append(num)
    return result


def blocking(num, size_num, size_block):
    return [int.from_bytes(num.to_bytes(size_num, 'big')[i * size_block: (i + 1) * size_block], 'big') for i in
            range(size_num // size_block)]


def decrypt(C, d, n):
    bytes_val = blocking(C, 32, 4)
    res = bytearray()
    for num in bytes_val:
        res += pow(num, d, n).to_bytes(8, 'big')
    return res


def encrypt(P, e, n):
    bytes_val = blocking(P, 64, 8)
    res = bytearray()
    for num in bytes_val:
        res += pow(num, e, n).to_bytes(4, 'big')
    return res


# Вариант 5
n = 517776452420107
e = 12377
C = 50527945233429
#C = 232174902406749442926770512941299737185022728

print('Зашифрованный текст:', C)

p, q = factor(n)
print('p =', p, ', q =', q)
f = (p - 1) * (q - 1)
print('f =', f)

d = invmod(e, f)
print('d =', d)
P = decrypt(C, d, n)
P_int = int.from_bytes(P, 'big')
print('Расшифрованый текст:', P_int)

#P_text = codecs.decode(P, 'latin') #P.decode( 'UTF-8', 'replace')
P_text = binascii.b2a_qp(P)


print('Расшифрованый текст:',  P_text)

C1 = encrypt(P_int, e, n)
C1 = int.from_bytes(C1, 'big')
print('<Оригинал> Зашифрованный текст:', C)
print('<Проверка> Зашифрованный текст:', C1)
print('<Совпадение>:', C1 == C)
