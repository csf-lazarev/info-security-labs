import binascii

from sympy.ntheory import factorint


def bezout(a, b):
    '''A recursive implementation of extended Euclidean algorithm.
    Returns integer x, y and gcd(a, b) for Bezout equation:
        ax + by = gcd(a, b).
    '''
    if not b:
        return 1, 0, a
    y, x, g = bezout(b, a % b)
    return x, y - (a // b) * x, g


def invmod(a, m):
    x, y, g = bezout(a, m)
    return None if g > 1 else (x % m + m) % m


def get_blocks(num, max):
    s_num = str(num)
    s_max = str(max)

    out = []

    i = 0
    l = len(s_max)
    while i < len(s_num):
        if s_num[0:l] > s_max:
            out.append(int(s_num[i: i + l - 1]))
            i = i + l - 1
        else:
            out.append(int(s_num[i: i + l]))
            i = i + l
    return out


def decrypt(block, d, n):
    return pow(block, d, n)


def get_codes(num):
    s_num = str(num)
    out = []
    for i in range(0, len(s_num) - 1, 2):
        out.append(int(s_num[i: i + 2]))
    if len(s_num) % 2 == 1:
        out.append(int(s_num[-1]))
    return out


def decrypt_main(msg, d, n):
    out = ""
    for m in msg:
        for b in get_blocks(m, n):
            out = out + "".join(one_block_decrypt(b, d, n))
    print(out)


def encrypt_main(array_blocks, e, n):
    for block in array_blocks:
        print(int("".join([str(decrypt(b, e, n)) for b in block])))


def one_block_decrypt(b, d, n):
    dd = decrypt(b, d, n)
    return [chr(c) for c in get_codes(dd)]


# 20
n = 274611845366113
e = 23311
enc = [108230462382949240744446393133139920760825242128635453394626156290136879344]

p, q = list(factorint(n).keys())[0], list(factorint(n).keys())[1]
print(p, q)

euler = (p - 1) * (q - 1)

print(euler)
d = invmod(e, euler)
print(d)
print((e * d) % euler == 1)
decrypt_main(enc, d, n)
encrypt_main([[decrypt(b, d, n) for b in get_blocks(bl, n)] for bl in enc], e, n)
