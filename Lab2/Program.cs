﻿using System;
using System.ComponentModel;

namespace Rextester
{
    public class Program
    {
        // Объявление констант
        const int N = 8; // Число раундов
        const UInt16 F16 = 0xFFFF; // 32 разрядное число
        private static bool debug = false;

        // Для реализации различных режимов шифрования (лаб 2)
        // Вектор для дополнительной шифровки первого блока сообщения (в режимах CBC и OFB)
        static UInt16 IV = 0x18FD; // инициализационный вектор

        // Исходное сообщение
        static UInt64 msg = 0x123456789ABCDEF0;
        static UInt64 K = 0x96EA704CFB1CF672; // исходный ключ (64 битный)

        // Циклический сдвиг вправо для 32 бит
        static UInt32 CycleShiftRight32(UInt32 x, int t)
        {
            return ((x >> t) | (x << (32 - t)));
        }

        // Циклический сдвиг вправо для 64 бит
        static UInt64 CycleShiftRight64(UInt64 x, int t)
        {
            return ((x >> t) | (x << (64 - t)));
        }

        // Циклический сдвиг влево для 32 бит
        static UInt32 CycleShiftLeft32(UInt32 x, int t)
        {
            return ((x << t) | (x >> (32 - t)));
        }

        static UInt16 CycleShiftLeft16(UInt16 x, int t)
        {
            return (UInt16) ((x << t) | (x >> (16 - t)));
        }

        static UInt16 CycleShiftRight16(UInt16 x, int t)
        {
            return (UInt16) ((x >> t) | (x << (16 - t)));
        }


        static UInt16[] SeparateBlock(UInt64 block)
        {
            UInt16[] blocks = new UInt16[4];
            for (int i = 0; i < 4; ++i)
            {
                blocks[i] = (UInt16) (block >> (16 * i));
            }

            return blocks.Reverse().ToArray();
        }

        static UInt64 BuildFrom16(UInt16[] blocks)
        {
            UInt64 res = blocks[0];
            for (int i = 1; i < 4; ++i)
            {
                res = (res << 16) | (uint) (blocks[i] & F16);
            }

            return res;
        }


        // Генерация 32 разрядного ключа на i-м раунде из исходного 64-разрядного
        static UInt32 GenRoundKey32(int round)
        {
            return (UInt32) CycleShiftRight64(K, round * 8); // циклический сдвиг на 8 бит и обрезка правых 32 бит
        }

        static UInt16 GenRoundKey16(UInt64 key,  int round)
        {
            return (UInt16) CycleShiftRight64(key, round * 8);
        }

        // Образующая функция - функция, шифрующая половину блока polblok ключом K_i на i-м раунде
        static UInt32 F(UInt32 halfOfBlock, UInt32 roundKey_I)
        {
            UInt32 f1 = CycleShiftLeft32(halfOfBlock, 9);
            UInt32 f2 = CycleShiftRight32(roundKey_I, 11) | halfOfBlock;
            return f1 ^ f2;
        }

        static UInt16 GenFunction(UInt16 x1, UInt16 x2, UInt16 x3, UInt16 key)
        {
            UInt16 f1 = (UInt16) (CycleShiftLeft16(key, 1) | (x1 & x2 & x3));
            UInt16 f2 = CycleShiftRight16(x1, 3);
            UInt16 f3 = CycleShiftRight16(x1, 5);
            UInt16 f4 = CycleShiftRight16(x1, 7);
            return (UInt16) (f1 ^ f2 ^ f3 ^ f4);
        }

        // Шифрование 64 разрядного блока
        static UInt64 shifr(UInt64 blok, UInt64 key)
        {
            var blocks = SeparateBlock(blok);

            // Выполняются 8 раундов шифрования
            for (int i = 0; i < N; i++)
            {
                UInt16 roundKeyI = GenRoundKey16(key, i);
                blocks[3] = (UInt16) (blocks[3] ^ GenFunction(blocks[0], blocks[1], blocks[2], roundKeyI));
                if (debug)
                {
                    Console.WriteLine($"Iter {i}");
                    foreach (var block in blocks)
                    {
                        Console.WriteLine("in {0:X}", block);
                    }

                    Console.WriteLine($"Iter {i} done");
                }

                // Если раунд не последний, то
                if (i < N - 1)
                {
                    var nBlock = new UInt16[4];
                    blocks.CopyTo(nBlock, 0);
                    blocks[0] = nBlock[1];
                    blocks[1] = nBlock[2];
                    blocks[2] = nBlock[3];
                    blocks[3] = nBlock[0];
                }
            }

            return BuildFrom16(blocks);
        }

        // Расшифровка 64 разрядного блока
        static UInt64 rasshifr(UInt64 blok, UInt64 key)
        {
            var blocks = SeparateBlock(blok);

            // Выполняются 8 раундов шифрования
            for (int i = N - 1; i >= 0; i--)
            {
                UInt16 roundKeyI = GenRoundKey16(key, i);
                blocks[3] = (UInt16) (blocks[3] ^ GenFunction(blocks[0], blocks[1], blocks[2], roundKeyI));
                if (debug)
                {
                    Console.WriteLine($"Iter {i}");
                    foreach (var block in blocks)
                    {
                        Console.WriteLine("in {0:X}", block);
                    }

                    Console.WriteLine($"Iter {i} done");
                }

                // Если раунд не последний, то
                if (i > 0)
                {
                    var nBlock = new UInt16[4];
                    blocks.CopyTo(nBlock, 0);
                    blocks[0] = nBlock[3];
                    blocks[1] = nBlock[0];
                    blocks[2] = nBlock[1];
                    blocks[3] = nBlock[2];
                }
            }

            return BuildFrom16(blocks);
        }

        static UInt64[] ReadFromFile(string path)
        {
            var readAllBytes = File.ReadAllBytes(path);
            var bytesLength = readAllBytes.Length;
            var blocks = bytesLength / sizeof(UInt64);
            var tail = bytesLength % sizeof(UInt64);
            var arrayLength = blocks + (tail > 0 ? 1 : 0);
            UInt64[] array = new UInt64[arrayLength];
            for (var blockInd = 0; blockInd < blocks; blockInd++)
            {
                array[blockInd] = BitConverter.ToUInt64(readAllBytes, blockInd * sizeof(UInt64));
            }

            if (tail > 0)
            {
                byte[] tailArray = new byte[8];
                Array.Fill(tailArray, Byte.MinValue);
                Array.Copy(readAllBytes, blocks * 8, tailArray, 0, tail);
                array[blocks] = BitConverter.ToUInt64(tailArray, 0);
            }

            return array;
        }

        static UInt64[] EncryptCBC(UInt64[] message, UInt64 initialVector)
        {
            UInt64[] msg_cbc = new UInt64[message.Length];
            Console.WriteLine("\nShifr CBC:");
            // Первый блок сообщения xor'ится с IV перед шифрованием:
            UInt64 blok = message[0] ^ initialVector;
            msg_cbc[0] = shifr(blok, K); // шифруем блок
            Console.Write("{0:X} ", msg_cbc[0]); // выводим зашифрованный первый блок на консоль

            // Каждый последующий блок перед шифрованием xor'ится с предыдущим зашифрованным блоком:
            for (int b = 1; b < message.Length; b++)
            {
                blok = message[b] ^ msg_cbc[b - 1]; // xor с предыдущим зашифрованным
                msg_cbc[b] = shifr(blok, K); // шифруем блок
                Console.Write("{0:X} ", msg_cbc[b]); // выводим зашифрованный блок на консоль
                // В зашифрованном тексте все блоки будут разными, не смотря на то что в исходном сообщении они повторялись
            }

            return msg_cbc;
        }

        static UInt64[] DecryptCBC(UInt64[] encoded, UInt64 initVector)
        {
            UInt64[] msg_cbc = new UInt64[encoded.Length];
            Console.WriteLine("\nText CBC:");
            // Первый блок сообщения xor'ится с IV после расшифровки:
            UInt64 msg_b = rasshifr(encoded[0], K); // расшифровка блока
            msg_b ^= IV; // xor'им с IV после расшифровки
            msg_cbc[0] = msg_b;
            Console.Write("{0:X} ", msg_b); // выводим расшифрованный первый блок на консоль
            // Каждый последующий блок после расшифровки xor'ится с предыдущим зашифрованным блоком:
            for (int b = 1; b < encoded.Length; b++)
            {
                msg_b = rasshifr(encoded[b], K); // расшифровка блока
                msg_b ^= encoded[b - 1]; // xor с предыдущим зашифрованным
                msg_cbc[b] = msg_b;
                Console.Write("{0:X} ", msg_b); // выводим расшифрованный блок на консоль
            }

            return msg_cbc;
        }
        
        static UInt64[] EncryptCBF(UInt64[] message, UInt64 initialVector)
        {
            UInt64[] msg_cbc = new UInt64[message.Length];
            Console.WriteLine("\nShifr CBF:");
            UInt64 blok = shifr(initialVector, K);
            msg_cbc[0] = blok ^ message[0];
            Console.Write("{0:X} ", msg_cbc[0]);
            
            for (int b = 1; b < message.Length; b++)
            {
                blok = shifr(msg_cbc[b - 1], K);
                msg_cbc[b] = blok ^ message[b];
                Console.Write("{0:X} ", msg_cbc[b]);
            }

            return msg_cbc;
        }

        static UInt64[] DecryptCBF(UInt64[] encoded, UInt64 initVector)
        {
            UInt64[] msg_cbf = new UInt64[encoded.Length];
            Console.WriteLine("\nText CBC:");
            UInt64 msg_b = shifr(initVector, K);
            msg_b ^= encoded[0];
            msg_cbf[0] = msg_b;
            Console.Write("{0:X} ", msg_b);
            for (int b = 1; b < encoded.Length; b++)
            {
                msg_b = shifr(encoded[b-1], K);
                msg_b ^= encoded[b];
                msg_cbf[b] = msg_b;
                Console.Write("{0:X} ", msg_b);
            }

            return msg_cbf;
        }

        static string From64Array(UInt64[] array)
        {
            var bytes = array.SelectMany(BitConverter.GetBytes).ToArray();
            return System.Text.Encoding.Default.GetString(bytes);
        }

        public static void Main(string[] args)
        {
            // Отображаем на консоли исходный ключ K (и IV) для зашифровки и исходное сообщение text (все это объявлено в первых строках)
            Console.WriteLine("Init Key {0:X}",
                K); // большой ключ K (64 бит) из битов которого создаются маленькие ключи K_i (по 32 бита)

            Console.WriteLine("Init V {0:X}",
                IV); // дополнительный ключ (вектор) для шифровки первого блока сообщения врежимах CBC и OFB (64 бит)
            var msg = ReadFromFile("test.txt");
            // Вывод блоков сообщения до шифрования
            Console.WriteLine("Text (message blocks)");
            var B = msg.Length;
            for (int b = 0; b < B; b++)
                Console.Write("{0:X} ", msg[b]); // выводим очередной блок сообщения

            var encodedCBC =  EncryptCBC(msg, IV);
            DecryptCBC(encodedCBC, IV);
            var encodedCBF = EncryptCBF(msg, IV);
            DecryptCBF(encodedCBF, IV);

        }
    }
}