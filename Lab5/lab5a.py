from functools import reduce

from Lab5.decrypt_dict import decrypt_dict

russianLetterFreq = {
    'О': 11.18,
    'Е': 8.95,
    'А': 7.64,
    'И': 7.09,
    'Н': 6.78,
    'Т': 6.09,
    'С': 4.97,
    'Л': 4.96,
    'В': 4.38,
    'Р': 4.23,
    'К': 3.30,
    'М': 3.17,
    'Д': 3.09,
    'П': 2.47,
    'Ы': 2.36,
    'У': 2.22,
    'Б': 2.01,
    'Я': 1.96,
    'Ь': 1.84,
    'Г': 1.72,
    'З': 1.48,
    'Ч': 1.40,
    'Й': 1.21,
    'Ж': 1.01,
    'Х': 0.95,
    'Ш': 0.72,
    'Ю': 0.47,
    'Ц': 0.39,
    'Э': 0.36,
    'Щ': 0.30,
    'Ф': 0.21,
    'Ъ': 0.02
}

russian_alphabet_set = "".join(russianLetterFreq.keys()).lower()


def remove_all_except_char_and_space(text):
    out = ""
    for l in text.lower():
        if l == " " or l in russian_alphabet_set:
            out = out + l
    return out


def remove_all_except_char(text):
    out = ""
    for l in text.lower():
        if l in russian_alphabet_set:
            out = out + l
    return out


def get_char_freq(text):
    map = {i: 0 for i in russian_alphabet_set}
    for l in text.lower():
        map[l] = map[l] + 1
    return map


def freq_map_to_percent(map, count):
    out = dict()
    for k, v in map.items():
        out[k] = round((v / count) * 100, 4)
    return {k: v for k, v in sorted(out.items(), key=lambda item: item[1], reverse=True)}


def get_popular_bigram(top_count, map, text):
    out = []
    sorted_map = {k: v for k, v in sorted(map.items(), key=lambda item: item[1], reverse=True)}
    bigrams = dict()
    in_letters = [k for k in list(sorted_map.keys())[:top_count]]
    for i, l in enumerate(text.lower()[:-1]):
        if text[i + 1] in russian_alphabet_set and l in in_letters:
            bg = f"{l}{text[i + 1]}"
            bigrams[bg] = bigrams.get(bg, 0) + 1
    return {k: v for k, v in sorted(bigrams.items(), key=lambda item: item[1], reverse=True)}


def get_short_words(max_length, text: str):
    t = text.lower().replace(r"\s+", " ")
    return list(sorted(list(set(filter(lambda w: 0 < len(w) <= max_length, t.split(" ")))), key=lambda w: len(w)))


def decrypt_text(text: str, ddict: dict):
    out = ""
    for i, l in enumerate(text):
        if l in ddict:
            out = out + "\033[34m{}\033[0m".format(ddict[l])
        elif l.lower() in ddict:
            out = out + "\033[34m{}\033[0m".format(ddict[l.lower()].upper())
        else:
            out = out + l
    return out


with open("rus_text.txt", 'r') as in_file:
    txt = in_file.read()
    lower_text = txt.lower()
    without_no_char = remove_all_except_char_and_space(lower_text)
    concatenated = remove_all_except_char(lower_text)
    freq = get_char_freq(concatenated)
    print("Popular bigram:")
    pb = get_popular_bigram(3, freq, without_no_char)
    print(pb)
    print("Freq:")
    fm = freq_map_to_percent(freq, len(concatenated))
    print(fm)
    d = decrypt_text(txt, decrypt_dict)
    print("Short words:")
    sw = get_short_words(3, without_no_char)
    print(sw)
    print("Text:")
    print(txt)
    print("Decoded:")
    print(d)
    print()
