﻿using System;
using System.ComponentModel;
using System.Text;

namespace Rextester
{
    public class Program
    {
        // Объявление констант
        const int N = 8; // Число раундов
        const UInt16 F16 = 0xFFFF;
        private static bool debug = false;

        // Исходное сообщение
        static UInt64 msg = 0x123456789ABCDEF0;
        static UInt64 K = 0x96EA704CFB1CF672;

        // Циклический сдвиг вправо для 32 бит
        static UInt32 CycleShiftRight32(UInt32 x, int t)
        {
            return ((x >> t) | (x << (32 - t)));
        }

        // Циклический сдвиг вправо для 64 бит
        static UInt64 CycleShiftRight64(UInt64 x, int t)
        {
            return ((x >> t) | (x << (64 - t)));
        }

        // Циклический сдвиг влево для 32 бит
        static UInt32 CycleShiftLeft32(UInt32 x, int t)
        {
            return ((x << t) | (x >> (32 - t)));
        }
        
        static UInt16 CycleShiftLeft16(UInt16 x, int t)
        {
            return (UInt16) ((x << t) | (x >> (16 - t)));
        }

        static UInt16 CycleShiftRight16(UInt16 x, int t)
        {
            return (UInt16) ((x >> t) | (x << (16 - t)));
        }


        static UInt16[] SeparateBlock(UInt64 block)
        {
            UInt16[] blocks = new UInt16[4];
            for (int i = 0; i < 4; ++i)
            {
                blocks[i] = (UInt16) (block >> (16 * i));
            }

            return blocks.Reverse().ToArray();
        }

        static UInt64 BuildFrom16(UInt16[] blocks)
        {
            UInt64 res = blocks[0];
            for (int i = 1; i < 4; ++i)
            {
                res = (res << 16) | (uint) (blocks[i] & F16);
            }

            return res;
        }


        // Генерация 32 разрядного ключа на i-м раунде из исходного 64-разрядного
        static UInt32 GenRoundKey32(int round)
        {
            return (UInt32) CycleShiftRight64(K, round * 8); // циклический сдвиг на 8 бит и обрезка правых 32 бит
        }

        static UInt16 GenRoundKey16(int round)
        {
            return (UInt16) CycleShiftRight64(K, round * 3);
        }
        
        static UInt16 GenFunction(UInt16 x1, UInt16 x2, UInt16 key)
        {
            UInt16 f1 = (UInt16) (CycleShiftLeft16(key, 1) | (x1 & x2));
            UInt16 f2 = CycleShiftRight16(x1, 3);
            UInt16 f3 = CycleShiftRight16(x2, 5);
            return (UInt16) (f1 ^ f2 ^ f3);
        }

        // Шифрование 64 разрядного блока
        static UInt64 shifr(UInt64 blok)
        {
            var blocks = SeparateBlock(blok);

            // Выполняются 8 раундов шифрования
            for (int i = 0; i < N; i++)
            {
                UInt16 roundKeyI = GenRoundKey16(i);
                blocks[2] = (UInt16) (blocks[2] ^ GenFunction(blocks[0], blocks[1], roundKeyI));
                if (debug)
                {
                    Console.WriteLine($"Iter {i}");
                    foreach (var block in blocks)
                    {
                        Console.WriteLine("in {0:X}", block);
                    }
                    Console.WriteLine($"Iter {i} done");
                }

                // Если раунд не последний, то
                if (i < N - 1)
                {
                    var nBlock = new UInt16[4];
                    blocks.CopyTo(nBlock, 0);
                    blocks[0] = nBlock[1];
                    blocks[1] = nBlock[2];
                    blocks[2] = nBlock[3];
                    blocks[3] = nBlock[0];
                }
            }

            return BuildFrom16(blocks);
        }

        // Расшифровка 64 разрядного блока
        static UInt64 rasshifr(UInt64 blok)
        {
            var blocks = SeparateBlock(blok);

            // Выполняются 8 раундов шифрования
            for (int i = N - 1; i >= 0; i--)
            {
                UInt16 roundKeyI = GenRoundKey16(i);
                blocks[2] = (UInt16) (blocks[2] ^ GenFunction(blocks[0], blocks[1], roundKeyI));
                if (debug)
                {
                    Console.WriteLine($"Iter {i}");
                    foreach (var block in blocks)
                    {
                        Console.WriteLine("in {0:X}", block);
                    }
                    Console.WriteLine($"Iter {i} done");
                }

                // Если раунд не последний, то
                if (i > 0)
                {
                    var nBlock = new UInt16[4];
                    blocks.CopyTo(nBlock, 0);
                    blocks[0] = nBlock[3];
                    blocks[1] = nBlock[0];
                    blocks[2] = nBlock[1];
                    blocks[3] = nBlock[2];
                }
            }

            return BuildFrom16(blocks);
        }

        static UInt64[] ReadFromBytes(byte[] bytes)
        {
            var bytesLength = bytes.Length;
            var blocks = bytesLength / sizeof(UInt64);
            var tail = bytesLength % sizeof(UInt64);
            var arrayLength = blocks + (tail > 0 ? 1 : 0);
            UInt64[] array = new UInt64[arrayLength];
            for (var blockInd = 0; blockInd < blocks; blockInd++)
            {
                array[blockInd] = BitConverter.ToUInt64(bytes, blockInd * sizeof(UInt64));
            }

            if (tail > 0)
            {
                byte[] tailArray = new byte[8];
                Array.Fill(tailArray, Byte.MinValue);
                Array.Copy(bytes, blocks * 8, tailArray, 0, tail);
                array[blocks] = BitConverter.ToUInt64(tailArray, 0);
            }

            return array;
        }

        static UInt64[] ReadFromFile(string path)
        {
            return ReadFromBytes(File.ReadAllBytes(path));
        }
        
        static UInt64[] ReadFromString(string str)
        {
            return ReadFromBytes(Encoding.UTF8.GetBytes(str));
        }

        static string From64Array(UInt64[] array)
        {
            var bytes = array.SelectMany(BitConverter.GetBytes).ToArray();
            return System.Text.Encoding.Default.GetString(bytes);
        }
        
        public static void Main(string[] args)
        {
            var src = ReadFromString(File.ReadAllText("test.txt"));
            Console.WriteLine("Source message - {0}", From64Array(src));
            var encoded = src.Select(shifr).ToArray();
            Console.WriteLine("Encoded message - {0}", From64Array(encoded));
            var decoded = encoded.Select(rasshifr).ToArray();
            Console.WriteLine("Decoded message - {0}", From64Array(decoded));
        }
    }
}