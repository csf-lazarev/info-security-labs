import math
import matplotlib.pyplot as plt

inv_pi = 1.0 / math.pi


def trig_with_counter(i, r0, r=16):
    return r0 if i == 0 else round(inv_pi * math.acos(math.cos(i + 100) * r0), r)
    # return r0 if i == 0 else inv_pi * math.acos(math.cos(i + 100) * r0)
    # return r0 if i == 0 else math.acos(math.cos(i + 100) * r0)
    # return r0 if i == 0 else math.acos(math.cos(i + 100) * r0) / math.pi


def gen_n(n, r0, p=12, start=0):
    return [trig_with_counter(i, r0, p) for i in range(start, start + n)]


def k_disp(k, q, c, r0, p, start=0):
    raw_data = gen_n(k * c, r0, p, start)
    vectors_raw = [raw_data[(i - 1) * k: i * k] for i in range(1, c)]
    t = 1.0 / q
    coords = [[int(x / t) for x in v] for v in vectors_raw]

    N = c
    M = q ** k
    m = {i: 0 for i in range(M)}

    for crd in coords:
        aim = 0
        for (index, cx) in enumerate(crd):
            aim = aim + cx * (q ** index)
        m[aim] = m.get(aim, 0) + 1

    NM = float(N) / M

    xi = sum([(mi - NM) ** 2 for mi in m.values()])
    xi = xi / NM
    return xi


def show_test_K():
    # for p = 0.05, v = 2, 14,
    real_xi = [6.0, 23.7, 0, 0]
    xi = test_k_iter()
    fig, ax = plt.subplots(nrows=2, ncols=2)
    for i, xii in enumerate(xi):
        x = [i * 200 for i in range(10)]
        y = [a[0] for a in xii]
        ax[i // 2][i % 2].plot(x, y, '-o')
        if real_xi[i]:
            ax[i // 2][i % 2].axhline(real_xi[i], color='red')
        ax[i // 2][i % 2].title.set_text(f"K = {i + 1}, M = {xii[0][1]}")
    plt.show()


def test_after_comma(r0, p, d, N):
    values = gen_n(N, r0, p, 0)
    map = {i: 0 for i in range(10)}
    for v in values:
        d_ = int(v * (10 * d)) % 10
        map[d_] = map[d_] + 1
    return map


def test_L(r0, p=12):
    generated = set()
    k = 0
    while True:
        g = trig_with_counter(k, r0, p)
        if g in generated:
            return k, g
        else:
            generated.add(g)
            k = k + 1


def test_k_iter():
    k = [1, 2, 3, 4]
    c = 200
    q = 4
    xi_res = [[(k_disp(k=ki, q=q, c=c, r0=0.9, p=10, start=si), q ** ki - 2) for si in range(0, ki * c * 10, ki * c)]
              for ki in k]
    return xi_res


def show_test_d(r0, p, d):
    N = 1000
    t = test_after_comma(r0, p, d, N)
    y = [v for k, v in t.items()]
    x = [i for i in range(10)]

    NM = N / 10

    xi = sum([(mi - NM) ** 2 for mi in y])
    xi = xi / NM

    fig, ax = plt.subplots(facecolor='white', dpi=80)
    ax.vlines(x=x, ymin=0, ymax=y, color='firebrick', alpha=0.7, linewidth=20)
    ax.title.set_text(f"r0 = {r0}, digit = {d}, xi = {xi}")

    # Annotate Text
    for i, cty in enumerate(y):
        ax.text(i, cty + 0.5, round(cty, i), horizontalalignment='center')
    plt.show()


def test_l(r0, L):
    k = L + 1
    x = trig_with_counter(L, r0)
    print(f"Find next {x} value..")
    while True:
        g = trig_with_counter(k, r0)
        if g == x:
            return k, g
        else:
            k = k + 1


def show_test_L():
    with open("testL.txt", 'r') as f:
        x, y = list(), list()
        for row in f:
            xi, yi = row.split(',')
            x.append(float(xi))
            y.append(int(yi))
        plt.plot(x, y)
        plt.show()


if __name__ == '__main__':
    # r0 = 0.3
    # while r0 < 1.0:
    #     print(f"For {r0} ...")
    #     tL = test_L(r0)
    #     print(*tL)
    #     tl = test_l(r0, tL[0])
    #     print(*tl)
    #     print("==============")
    #     r0 = r0 + 0.1

    # d1 = trig_with_counter(74724406, 0.99)
    # d2 = trig_with_counter(336833381, 0.99)
    # s1 = "{:.12f}".format(d1)
    # s2 = "{:.12f}".format(d2)
    # print(s1)
    # print(s2)
    # print(s1 == s2)
    # print(d1 == d2)
    # print(test_L(0.9, 14))
    show_test_L()
    show_test_K()
    show_test_d(0.9, 8, 1)
    show_test_d(0.1, 8, 1)
    show_test_d(0.9, 8, 3)
    show_test_d(0.5, 8, 1)
    show_test_d(0.5, 8, 3)
